import Vue from 'vue'
import axios from 'axios'
import 'ui-js-tree/css/ui-js-tree.css'
import 'ag-grid/dist/styles/ag-grid.css'
import 'ag-grid/dist/styles/ag-theme-material.css'
import './prismtheme.css'
import { AsyncDataPlugin } from 'vue-async-data-2'
import underscore from 'underscore'
import vClickOutside from 'v-click-outside'


import App from './App'
import router from './router'
import store from './store'

// Vue.use(require('ui-js-tree/'))
Vue.use(AsyncDataPlugin)
Vue.use(vClickOutside)

if (!process.env.IS_WEB) Vue.use(require('vue-electron'))
process.env.ELECTRON_DISABLE_SECURITY_WARNINGS = '1'
Vue.http = Vue.prototype.$http = axios
Vue.config.productionTip = false
Vue.config.silent = true
Vue.$_ = Vue.prototype.$_ = underscore

/* eslint-disable no-new */
Vue.EventBus = new Vue({
  components: { App },
  router,
  store,
  template: '<App/>'
}).$mount('#app')
