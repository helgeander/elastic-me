import Vue from 'vue'
import UiJsTree from 'ui-js-tree'

Vue.directive('uitree', {
  // When the bound element is inserted into the DOM...
  inserted: (el, binding, vnode) => {
    var treeOptions = binding.value.options
    var treeData = binding.value.data
    vnode.context.tree = new UiJsTree(treeData, el, treeOptions)

    vnode.context.tree.render()
  },
  update: (el, binding, vnode) => {
    var treeData = binding.value.data
    if (vnode.context.tree.data !== treeData) {
      vnode.context.tree.load(treeData)
    }
  }
})
