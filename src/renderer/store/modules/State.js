import Vue from 'vue'

const state = {
  View: {}
}

const mutations = {
  UpdateViewState (state, viewState) {
    let existingState = state.View || {}
    state.View= Vue.$_.extend(existingState, viewState)
    sessionStorage.setItem("viewState", JSON.stringify(state.View))
  },
  SetViewState (state, viewState) {
    state.View = viewState
  }
}

const actions = {
  fetchViewState ({ commit }) {
    commit('SetViewState', JSON.parse(sessionStorage.getItem("viewState")) || {})
  }
}

const getters = {
  getViewState (state) {
    return (viewName) => {
      return state.View[viewName]
    }
  }
}

export default {
  state,
  mutations,
  actions,
  getters
}
