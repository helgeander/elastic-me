import Vue from 'vue'
import electron from 'electron'
let settings = JSON.parse(electron.remote.getGlobal('config').settings)
let serverName = settings.selected
let elasticUri = settings.servers[serverName].uri

const state = {
  info: null,
  count: null,
  nodes: null,
  stats: null,
  indices: null,
  aliases: null,
  currentDocument: {},
  currentIndex: {
    aliases: {},
    mappings: {},
    settings: { index: {} }
  },
  uri: elasticUri
}

const mutations = {
  UpdateInfo (state, infoPromise) {
    state.info = infoPromise
  },
  UpdateStats (state, statsPromise) {
    state.stats = statsPromise
  },
  UpdateIndices (state, statsPromise) {
    state.indices = statsPromise
  },
  UpdateNodes (state, nodesPromise) {
    state.nodes = nodesPromise
  },
  UpdateAliases (state, aliasPromise) {
    state.aliases = aliasPromise
  },
  UpdateCurrentIndex (state, indexPromise) {
    state.currentIndex = indexPromise
  },
  UpdateCurrentDocument (state, docPromise) {
    state.currentDocument = docPromise
  }
}

function commitPromise(path, key, resultFunc, commit) {
  let uri = `${elasticUri}/${path}`,
      promise = Vue.http.get(uri).then(resultFunc)
    commit(key, promise)
    return promise
}

const actions = {
  fetchInfo ({ commit }) {
    return commitPromise('', 'UpdateInfo', r => r.data, commit)
  },
  fetchNodes ({ commit }) {
    return commitPromise('_nodes', 'UpdateNodes', r => r.data._nodes, commit)
  },
  fetchStats ({ commit }) {
    return commitPromise('_stats', 'UpdateStats', r => r.data, commit)
  },
  fetchIndices ({ commit }) {
    return commitPromise('_stats/indexing', 'UpdateIndices', r => r.data, commit)
  },
  fetchAliases ({ commit }) {
    return commitPromise('_aliases', 'UpdateAliases', r => r.data, commit)
  },
  fetchIndex ({ commit }, { index }) {
    return commitPromise(index, 'UpdateCurrentIndex', r => r.data, commit)
  },
  fetchDocument ({ commit }, { index, documentid, type }) {
    return commitPromise(`${index}/${type}/${documentid}`, 'UpdateCurrentDocument', r => r.data, commit)
  },

  createIndex ({ commit, dispatch }, { index, settings }) {
    return Vue.http.put(`${elasticUri}/${index}`, settings).then(r => {
      dispatch('fetchAliases')
      dispatch('fetchIndices')
      return r
    })
  },
  updateIndexMapping ({ commit, dispatch }, { index, mapping }) {
    return Vue.http.put(`${elasticUri}/${index}/_mapping/_doc`, mapping).then(r => {
      dispatch('fetchAliases')
      dispatch('fetchIndices')
      return r
    })
  },
  updateIndexSettings ({ commit, dispatch }, { index, settings }) {
    return Vue.http.put(`${elasticUri}/${index}/_settings`, settings).then(r => {
      dispatch('fetchAliases')
      dispatch('fetchIndices')
      return r
    })
  },
  deleteAll ({ commit, dispatch }) {
    return Vue.http.delete(`${elasticUri}/_all`).then(r => {
      dispatch('fetchAliases')
      dispatch('fetchIndices')
      return r
    })
  },
  deleteIndex ({ commit, dispatch }, { index }) {
    return Vue.http.delete(`${elasticUri}/${index}`).then(r => {
      dispatch('fetchAliases')
      dispatch('fetchIndices')
      return r
    })
  }
}

const getters = {
  getInfo (state) {
    return state.info
  },
  getStats (state) {
    return state.stats
  },
  getCurrentMapping (state) {
    return state.currentIndex.mappings
  },
  getCurrentSettings (state) {
    return state.currentIndex.settings.index
  },
  getDocument (state) {
    return state.currentDocument
  },
  getConnection () {
    return elasticUri
  }
}

export default {
  state,
  mutations,
  actions,
  getters
}
