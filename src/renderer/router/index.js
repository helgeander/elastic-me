import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'welcome-view',
      component: require('@/components/WelcomeView').default
    },
    {
      path: '*',
      redirect: '/'
    },
    {
      path: '/index/:index',
      name: 'index-view',
      component: require('@/components/IndexView').default,
      props: true
    },
    {
      path: '/createindex',
      name: 'create-index',
      component: require('@/components/CreateIndex').default
    }
  ]
})
